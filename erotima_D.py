import gmpy2

with open("GE3_keys.txt") as f:
    keys = f.readlines()
keys = [int(k.strip()) for k in keys]
for i in range(len(keys)):
    for j in range(i + 1, len(keys)):
        if gmpy2.gcd(keys[i], keys[j]) > 1:
            print("RSA modulo %d and %d have a common factor" % (i + 1, j + 1))
