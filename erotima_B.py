import random
import gmpy2
from time import time

def isprime(x):
    for i in range(20):
        a = random.randrange(x)
        if pow(a, x, x) != a:
            return False
    return True

def gcd(x, y):
    while (y):
        x, y = y, x % y
    return x

def fast_gen_prime(BITS):
    ub = 2 ** (BITS) // M
    lb = ub // 2
    while 1:
        r = random.randrange(lb, ub)
        p = M * r + 1
        if p.bit_length() == 256:
            if isprime(p):
                return p

#ΕΡΩΤΗΜΑ 3
primelst = [2]
while primelst[-1] < 166:
    primelst.append(gmpy2.next_prime(primelst[-1]))

M = 1
for i in range(20):
    M *= primelst[i]
    ts = time()

    totalSec = 0

    for j in range(100):
        p = fast_gen_prime(256)
        totalSec += time()-ts
    print("Iteration #", i+1, " fast_gen_prime for M:",
          M, "did: ", time()-ts, "seconds")
