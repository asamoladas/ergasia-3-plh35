import random
from time import time

def isprime(x):
    for i in range(20):
        a = random.randrange(x)
        if pow(a, x, x) != a:
            return False
    return True

def gcd(x, y):
    while (y):
        x, y = y, x % y
    return x

def gen_prime(BITS):
    ub = 2 ** (BITS)
    lb = ub // 2
    p = random.randrange(lb, ub)
    while not isprime(p):
        p = random.randrange(lb, ub)
    return p

#ΕΡΩΤΗΜΑ Α1
def fast_gen_prime(BITS):
    ub = 2 ** BITS // 2
    lb = ub // 2
    while 1:
        r = random.randrange(lb, ub)
        p = 2 * r + 1
        if isprime(p):
            return p

#ΕΡΩΤΗΜΑ Α2
def fast_gen_prime_2(BITS):
    ub = (2 ** BITS) // 6  # στρογγυλοποίηση στον κατω ακεραιο
    lb = (ub // 2)
    #print((6 * lb + 1).bit_length())
    while 1:
        r = random.randrange(lb, ub)
        p = 6 * r + 1
        if p.bit_length() == 256:
            if isprime(p):
                return p

ts = time()
for i in range(100):
    p = gen_prime(256)
te = time()
print("gen prime did", te - ts, "seconds")

ts = time()
for i in range(100):
    p = fast_gen_prime(256)
te = time()
print("gen fast_gen_prime did", te - ts, "seconds")

ts = time()
for i in range(100):
    p = fast_gen_prime_2(256)
te = time()
print("gen fast_gen_prime_2 did", te - ts, "seconds")
